package com.example.base.domain.interactor.scheduler

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

class IOThreadScheduler: ThreadScheduler {

    override fun scheduler(): Scheduler = Schedulers.io()
}