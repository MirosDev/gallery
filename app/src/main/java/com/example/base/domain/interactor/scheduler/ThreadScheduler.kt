package com.example.base.domain.interactor.scheduler

import io.reactivex.Scheduler

interface ThreadScheduler {

    fun scheduler(): Scheduler
}