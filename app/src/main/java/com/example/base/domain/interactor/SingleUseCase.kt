package com.example.base.domain.interactor

import com.example.base.domain.interactor.scheduler.ThreadScheduler
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver

/**
* Abstract class for a UseCase that returns an instance of a [Single].
*/
abstract class SingleUseCase<T, in Params> constructor(
    private val threadExecutor: ThreadScheduler,
    private val postThreadScheduler: ThreadScheduler) {

    private var disposables: CompositeDisposable? = null

    /**
    * Builds a [Single] which will be used when the current [SingleUseCase] is executed.
    */
    protected abstract fun buildUseCaseObservable(params: Params): Single<T>

    /**
    * Executes the current use case.
    */
    open fun execute(singleObserver: DisposableSingleObserver<T>, params: Params): Disposable {
        val single = this.buildUseCaseObservable(params)
            .subscribeOn(threadExecutor.scheduler())
            .observeOn(postThreadScheduler.scheduler()) as Single<T>
        val disposable = single.subscribeWith(singleObserver)
        addDisposable(disposable)
        return disposable
    }

    /**
    * Dispose from current [CompositeDisposable].
    */
    fun dispose() {
        disposables?.dispose()
        disposables = null
    }

    /**
     * Dispose from current [CompositeDisposable].
     */
    private fun addDisposable(disposable: Disposable) {
        if (disposables == null) {
            disposables = CompositeDisposable()
        }

        disposables?.add(disposable)
    }
}