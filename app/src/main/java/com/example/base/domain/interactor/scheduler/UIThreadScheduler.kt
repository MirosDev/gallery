package com.example.base.domain.interactor.scheduler

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

class UIThreadScheduler: ThreadScheduler {

    override fun scheduler(): Scheduler = AndroidSchedulers.mainThread()
}