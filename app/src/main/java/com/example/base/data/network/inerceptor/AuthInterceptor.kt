package com.example.base.data.network.inerceptor

import com.example.base.data.TokenResponse
import com.example.gallery.BuildConfig
import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response
import java.io.IOException

class AuthInterceptor(
    private val baseUrl: String,
    private val gson: Gson
) : Interceptor {

    private val mimeType = "application/json;charset=utf-8".toMediaTypeOrNull()

    //save token to native via JNI interface
    private var token: String = ""

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        val response = chain.proceedRequest(originalRequest)

        return if (response.code == 401) {
            response.close()
            token = ""

            val postBodyBody = RequestBody.create(mimeType, "{ \"apiKey\": \"${BuildConfig.API_KEY}\" }")

            val refreshTokenRequest = Request.Builder()
                .post(postBodyBody)
                .url("${baseUrl}auth")
                .build()

            val refreshTokenResponse = chain.proceed(refreshTokenRequest)

            if (refreshTokenResponse.isSuccessful) {
                val refreshedToken = gson.fromJson(
                    refreshTokenResponse.body?.string() ?: "",
                    TokenResponse::class.java
                )
                token = refreshedToken.token

                return chain.proceedRequest(originalRequest)
            } else {
                refreshTokenResponse.close()
                chain.proceed(chain.request())
            }
        } else {
            response
        }
    }

    private fun Interceptor.Chain.proceedRequest(r: Request): Response {
        val request = r.newBuilder()
            .addHeader("Authorization", "Bearer $token")
            .build()
        return proceed(request)
    }
}