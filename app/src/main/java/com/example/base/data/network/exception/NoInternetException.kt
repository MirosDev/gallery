package com.example.base.data.network.exception

import java.lang.RuntimeException

class NoInternetException : RuntimeException()