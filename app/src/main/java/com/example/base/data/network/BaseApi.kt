package com.example.base.data.network

import com.example.base.data.network.exception.NoInternetException
import com.example.gallery.data.network.NetworkProvider
import io.reactivex.Single

abstract class BaseApi(private val networkProvider: NetworkProvider) {

    protected fun <T> sendRequest(requestDelegate: () -> Single<T>): Single<T> =
        Single.defer {
            if (networkProvider.isOnline()) {
                requestDelegate.invoke()
            } else {
                Single.error(NoInternetException())
            }
        }
}