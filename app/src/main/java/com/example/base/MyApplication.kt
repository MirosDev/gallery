package com.example.base

import android.app.Application
import com.example.base.presentation.di.component.AppComponent

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        AppComponent.initAndGet(this)
    }
}