package com.example.base.presentation.di.injector

import com.example.base.presentation.di.component.AppComponent
import com.example.gallery.presentation.di.component.GalleryComponent
import com.example.gallery.presentation.di.component.GalleryComponentApi

class FeatureInjector {

    companion object {

        fun initGalleryComponent(): GalleryComponentApi = GalleryComponent.initAndGet(AppComponent.get())
    }
}