package com.example.base.presentation.di.module

import android.app.Application
import com.example.base.data.network.inerceptor.AuthInterceptor
import com.example.base.domain.interactor.qualifier.IOScheduler
import com.example.base.domain.interactor.qualifier.UIScheduler
import com.example.base.domain.interactor.scheduler.IOThreadScheduler
import com.example.base.domain.interactor.scheduler.ThreadScheduler
import com.example.base.domain.interactor.scheduler.UIThreadScheduler
import com.example.gallery.data.network.NetworkProvider
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
class AppModule(
    val app: Application
) {

    @Provides
    @Singleton
    fun provideNetworkProvider() = NetworkProvider(app)

    @Provides
    @Singleton
    @UIScheduler
    fun provideUIScheduler(): ThreadScheduler = UIThreadScheduler()

    @Provides
    @Singleton
    @IOScheduler
    fun provideIOScheduler(): ThreadScheduler = IOThreadScheduler()

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        return gsonBuilder
            .setLenient()
            .create()
    }

    @Provides
    @Singleton
    fun provideBaseUrl(): String = "http://interview.agileengine.com/"

    @Provides
    @Singleton
    fun provideOkHttp(baseUrl: String, gson: Gson): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        return OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor(AuthInterceptor(baseUrl, gson))
            .build()
    }

    @Provides
    @Singleton
    fun provideRxJava2CallAdapterFactory(): RxJava2CallAdapterFactory =
        RxJava2CallAdapterFactory
            .createWithScheduler(Schedulers.io())

    @Provides
    @Singleton
    fun provideRetrofit(
        baseUrl: String,
        gson: Gson,
        okHttpClient: OkHttpClient,
        rxJava2CallAdapterFactory: RxJava2CallAdapterFactory
    ): Retrofit =
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(rxJava2CallAdapterFactory)
            .client(okHttpClient)
            .build()
}