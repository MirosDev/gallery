package com.example.base.presentation.di.component

import android.app.Application
import com.example.base.presentation.di.module.AppModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
abstract class AppComponent : AppComponentApi {

    companion object : BaseDaggerComponentHolder<AppComponent>() {

        fun initAndGet(myApp: Application): AppComponentApi {
            super.incrementInstancesCount()
            if (component == null) {
                synchronized(AppComponent::class.java) {
                    if (component == null) {
                        component = DaggerAppComponent.builder()
                            .appModule(AppModule(myApp))
                            .build()
                    }
                }
            }

            return component!!
        }
    }
}