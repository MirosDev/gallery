package com.example.base.presentation.view_model

sealed class LoadingDataViewState<T>(
    val data: T?
) {

    class Data<T>(data: T) : LoadingDataViewState<T>(data)

    class Loading<T>(data: T? = null) : LoadingDataViewState<T>(data)

    class Error<T>(error: Throwable? = null, prevData: T?) : LoadingDataViewState<T>(prevData)
}