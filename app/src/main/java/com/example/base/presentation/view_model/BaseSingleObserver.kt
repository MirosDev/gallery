package com.example.base.presentation.view_model

import io.reactivex.observers.DisposableSingleObserver

open class BaseSingleObserver<T> : DisposableSingleObserver<T>() {

    override fun onSuccess(data: T) { }

    override fun onError(error: Throwable) { }
}