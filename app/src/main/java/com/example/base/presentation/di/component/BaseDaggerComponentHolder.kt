package com.example.base.presentation.di.component

import androidx.annotation.CallSuper
import java.util.concurrent.atomic.AtomicInteger

abstract class BaseDaggerComponentHolder<T> {

    @Volatile
    private var instanceCount = AtomicInteger(0)

    @Volatile
    protected var component: T? = null

    /**
     *  Must call inside 'initAndGet()' method
     */
    protected fun incrementInstancesCount() = instanceCount.incrementAndGet()

    fun get(): T = component
        ?: error("You must call 'initAndGet()' method")

    @CallSuper
    open fun resetComponent() {
        if (instanceCount.get() > 1) {
            instanceCount.getAndDecrement()
            return
        }

        instanceCount.set(0)
        component = null
    }
}