package com.example.base.presentation.di.component

import com.example.base.domain.interactor.qualifier.IOScheduler
import com.example.base.domain.interactor.qualifier.UIScheduler
import com.example.base.domain.interactor.scheduler.ThreadScheduler
import com.example.gallery.data.network.NetworkProvider
import retrofit2.Retrofit

interface AppComponentApi {

    fun provideRetrofit(): Retrofit

    @UIScheduler
    fun provideUIScheduler(): ThreadScheduler

    @IOScheduler
    fun provideIOScheduler(): ThreadScheduler

    fun provideNetworkProvider(): NetworkProvider
}