package com.example.gallery.presentation.view


import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.base.presentation.di.injector.FeatureInjector
import com.example.base.presentation.view_model.LoadingDataViewState
import com.example.gallery.R
import com.example.gallery.presentation.di.component.GalleryComponent
import com.example.gallery.presentation.model.ImagesModel
import com.example.gallery.presentation.model.PictureModel
import com.example.gallery.presentation.view.adapter.GalleryListAdapter
import com.example.gallery.presentation.view.adapter.GridDividerItemDecoration
import com.example.gallery.presentation.view_model.GalleryViewModelFactory
import com.example.gallery.presentation.view_model.ImageViewModel
import kotlinx.android.synthetic.main.fragment_gallery_list.*
import javax.inject.Inject

class GalleryListFragment : Fragment(R.layout.fragment_gallery_list) {

    @Inject
    lateinit var factory: GalleryViewModelFactory

    @Inject
    lateinit var glAdapter: GalleryListAdapter

    private lateinit var imageViewModel: ImageViewModel
    private lateinit var adapterCallback: GalleryListAdapter.Callback
    private lateinit var onScrollListener: RecyclerView.OnScrollListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        FeatureInjector.initGalleryComponent()
        GalleryComponent.get().inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        setupVM()

        subscribeToImagesViewState()
        imageViewModel.loadImages()
    }

    private fun setupUI() {
        gallery_list_srl.setOnRefreshListener {
            if (imageViewModel.getPictures().isEmpty()) {
                imageViewModel.loadImages()
            }
            gallery_list_srl.isRefreshing = false
        }

        onScrollListener = object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val layoutManager = recyclerView.layoutManager as GridLayoutManager
                val visibleItemCount = layoutManager.childCount
                val totalItemCount = layoutManager.itemCount
                val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

                val hasMore = imageViewModel.hasMore()

                if (hasMore &&
                    visibleItemCount + firstVisibleItemPosition >= totalItemCount &&
                    firstVisibleItemPosition >= 0
                ) {
                    imageViewModel.loadImages()
                }
            }
        }

        adapterCallback = object : GalleryListAdapter.Callback {

            override fun onItemClick(item: PictureModel) {
                val action = GalleryListFragmentDirections
                    .actionGalleryListFragmentToGalleryItemFragment(item.id)
                findNavController().navigate(action)
            }
        }

        gallery_list_rv.apply {
            adapter = glAdapter
            animation = null
            addItemDecoration(
                GridDividerItemDecoration(
                    resources.getDimension(R.dimen.grid_padding).toInt()
                )
            )
            layoutManager = GridLayoutManager(context, resources.getInteger(R.integer.grid_count))
        }
    }

    private fun setupVM() {
        imageViewModel =
            ViewModelProviders.of(requireActivity(), factory).get(ImageViewModel::class.java)
    }

    private fun subscribeToImagesViewState() {
        imageViewModel.imagesViewState.observe(viewLifecycleOwner, this::setImagesViewState)
    }

    private fun setImagesViewState(viewState: LoadingDataViewState<ImagesModel>) =
        when (viewState) {
            is LoadingDataViewState.Data -> {
                glAdapter.submitList(viewState.data!!.pictures)
            }
            is LoadingDataViewState.Loading -> {
            }
            is LoadingDataViewState.Error -> {
            }
        }

    override fun onResume() {
        super.onResume()
        glAdapter.callback = adapterCallback
        gallery_list_rv.addOnScrollListener(onScrollListener)
    }

    override fun onPause() {
        super.onPause()
        glAdapter.callback = null
        gallery_list_rv.removeOnScrollListener(onScrollListener)
    }
}