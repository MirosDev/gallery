package com.example.gallery.presentation.model

data class ImageDetailsModel(
    val id: String,
    val author: String,
    val camera: String?,
    val tags: String,
    val croppedPicture: String,
    val fullPicture: String
)