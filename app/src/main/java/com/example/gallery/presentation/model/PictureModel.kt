package com.example.gallery.presentation.model

data class PictureModel(
    val id: String,
    val croppedPicture: String
)