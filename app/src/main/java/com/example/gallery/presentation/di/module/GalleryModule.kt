package com.example.gallery.presentation.di.module

import com.example.base.presentation.di.scope.PerFeature
import com.example.gallery.data.GalleryRepository
import com.example.gallery.data.data_store.FactoryGalleryDataStore
import com.example.gallery.data.data_store.IGalleryDataStore
import com.example.gallery.data.data_store.RemoteGalleryDataStore
import com.example.gallery.data.network.api.GalleryApi
import com.example.gallery.data.network.api.IGalleryApi
import com.example.gallery.domain.repository.IGalleryRepository
import dagger.Binds
import dagger.Module

@Module
interface GalleryModule {

    @Binds
    @PerFeature
    fun api(api: GalleryApi): IGalleryApi

    @Binds
    @PerFeature
    fun remoteDaraStore(dataStore: RemoteGalleryDataStore): IGalleryDataStore

    //not necessary
    @PerFeature
    fun factory(factory: FactoryGalleryDataStore): FactoryGalleryDataStore

    @Binds
    @PerFeature
    fun repository(repository: GalleryRepository): IGalleryRepository
}