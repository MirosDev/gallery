package com.example.gallery.presentation.mapper

import com.example.base.data.mapper.BaseMapper
import com.example.gallery.domain.model.Images
import com.example.gallery.presentation.model.ImagesModel
import javax.inject.Inject

class ImagesMapper @Inject constructor(
    private val pictureMapper: PictureMapper
): BaseMapper<Images, ImagesModel>() {

    override fun map(from: Images) =
        ImagesModel(
            pictureMapper.map(from.pictures),
            from.page,
            from.pageCount,
            from.hasMore
        )

    override fun reverse(to: ImagesModel) =
        Images(
            pictureMapper.reverse(to.pictures),
            to.page,
            to.pageCount,
            to.hasMore
        )
}