package com.example.gallery.presentation.view_model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.base.presentation.view_model.BaseSingleObserver
import com.example.base.presentation.view_model.LoadingDataViewState
import com.example.gallery.domain.interactor.GetImagesUseCase
import com.example.gallery.domain.model.Images
import com.example.gallery.presentation.mapper.ImagesMapper
import com.example.gallery.presentation.model.ImagesModel
import javax.inject.Inject

class ImageViewModel @Inject constructor(
    private val imagesMapper: ImagesMapper,
    private val getImagesUseCase: GetImagesUseCase
) : ViewModel() {

    val imagesViewState = MutableLiveData<LoadingDataViewState<ImagesModel>>()

    fun loadImages() {
        imagesViewState.postValue(LoadingDataViewState.Loading(imagesViewState.value?.data))
        val observer = object : BaseSingleObserver<Images>() {

            override fun onSuccess(data: Images) {
                val dataModel = imagesMapper.map(data)
                val newDataModel = dataModel.copy(pictures = getPictures().plus(dataModel.pictures))

                imagesViewState.postValue(LoadingDataViewState.Data(newDataModel))
            }

            override fun onError(error: Throwable) {
                imagesViewState.postValue(
                    LoadingDataViewState.Error(
                        error,
                        imagesViewState.value?.data
                    )
                )
            }
        }

        getImagesUseCase.dispose()
        getImagesUseCase.execute(observer, GetImagesUseCase.Params(nextPage()))
    }

    fun getPictures() = imagesViewState.value?.data?.pictures ?: listOf()

    fun hasMore() = imagesViewState.value?.data?.hasMore ?: true

    private fun nextPage() = (imagesViewState.value?.data?.page ?: 0).inc()

    override fun onCleared() {
        super.onCleared()
        getImagesUseCase.dispose()
    }
}