package com.example.gallery.presentation.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.base.presentation.di.annotation.ViewModelKey
import com.example.base.presentation.di.scope.PerFeature
import com.example.gallery.presentation.view_model.GalleryViewModelFactory
import com.example.gallery.presentation.view_model.ImageDetailsViewModel
import com.example.gallery.presentation.view_model.ImageViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface GalleryVMModule {

    @Binds
    @PerFeature
    fun bindViewModelFactory(factory: GalleryViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ImageViewModel::class)
    fun bindImageViewModel(viewModel: ImageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ImageDetailsViewModel::class)
    fun bindImageDetailsViewModel(viewModel: ImageDetailsViewModel): ViewModel
}