package com.example.gallery.presentation.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.Glide
import com.example.gallery.R
import com.example.gallery.presentation.model.PictureModel
import kotlinx.android.synthetic.main.gallery_list_item.view.*
import javax.inject.Inject


class GalleryListAdapter @Inject constructor(

) : ListAdapter<PictureModel, GalleryListItemViewHolder>(GalleryListDiffUtilCallback()) {

    var callback: Callback? = null

    //Can add progress item when loading next page
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryListItemViewHolder =
        GalleryListItemViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.gallery_list_item, parent, false)
        )

    override fun onBindViewHolder(holder: GalleryListItemViewHolder, position: Int) {
        holder.bind(getItem(position), callback)
    }

    override fun onViewRecycled(viewHolder: GalleryListItemViewHolder) {
        Glide.with(viewHolder.itemView).clear(viewHolder.itemView.gallery_list_item_im)
    }

    interface Callback {

        fun onItemClick(item: PictureModel)
    }
}