package com.example.gallery.presentation.mapper

import com.example.base.data.mapper.BaseMapper
import com.example.gallery.domain.model.Picture
import com.example.gallery.presentation.model.PictureModel
import javax.inject.Inject

class PictureMapper @Inject constructor() : BaseMapper<Picture, PictureModel>() {

    override fun map(from: Picture) =
        PictureModel(
            from.id,
            from.croppedPicture
        )

    override fun reverse(to: PictureModel) = Picture(
        to.id,
        to.croppedPicture
    )
}