package com.example.gallery.presentation.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.base.presentation.view.OnSwipeFlyingListener
import com.example.base.presentation.view_model.LoadingDataViewState
import com.example.gallery.R
import com.example.gallery.presentation.di.component.GalleryComponent
import com.example.gallery.presentation.model.ImageDetailsModel
import com.example.gallery.presentation.view.extensions.buildTextIntent
import com.example.gallery.presentation.view.extensions.findNextImageId
import com.example.gallery.presentation.view.extensions.findPrevImageId
import com.example.gallery.presentation.view_model.GalleryViewModelFactory
import com.example.gallery.presentation.view_model.ImageDetailsViewModel
import com.example.gallery.presentation.view_model.ImageViewModel
import kotlinx.android.synthetic.main.fragment_gallery_details.*
import javax.inject.Inject

class GalleryDetailsFragment : Fragment(R.layout.fragment_gallery_details) {

    @Inject
    lateinit var factory: GalleryViewModelFactory

    private lateinit var imageDetailsViewModel: ImageDetailsViewModel
    private lateinit var imageViewModel: ImageViewModel

    private lateinit var id: String

    override fun setArguments(args: Bundle?) {
        super.setArguments(args)
        id = navArgs<GalleryDetailsFragmentArgs>().value.imageId
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        GalleryComponent.get().inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupVM()

        subscribeToImagesViewState()
        imageDetailsViewModel.loadImageDetails(id)

        if(imageViewModel.getPictures().last().id == id && imageViewModel.hasMore()) {
            imageViewModel.loadImages()
        }
    }

    private fun setupUI() {
        button_back_iv.setOnClickListener {
            findNavController().popBackStack()
        }

        fab.setOnClickListener {
            imageDetailsViewModel.getImageDetails()?.fullPicture?.let {
                val shareIntent: Intent = Intent().buildTextIntent(it)
                startActivity(Intent.createChooser(shareIntent, null))
            }
        }

        image_iv.setOnSingleFlingListener(object : OnSwipeFlyingListener() {

            override fun onSwipeLeft() {
                val nextImageId = imageViewModel.getPictures().findPrevImageId(id)
                if (nextImageId != -1) {
                    val action = GalleryDetailsFragmentDirections
                        .actionGalleryItemFragmentLeftSelf(imageViewModel.getPictures()[nextImageId].id)
                    findNavController().navigate(action)
                }
            }

            override fun onSwipeRight() {
                val nextImageId = imageViewModel.getPictures().findNextImageId(id)
                if (nextImageId != -1) {
                    val action = GalleryDetailsFragmentDirections
                        .actionGalleryItemFragmentRightSelf(imageViewModel.getPictures()[nextImageId].id)
                    findNavController().navigate(action)
                }
            }
        })
    }

    private fun setupVM() {
        imageDetailsViewModel =
            ViewModelProviders.of(this, factory).get(ImageDetailsViewModel::class.java)
        imageViewModel =
            ViewModelProviders.of(requireActivity(), factory).get(ImageViewModel::class.java)
    }

    private fun subscribeToImagesViewState() {
        imageDetailsViewModel.imageDetailsViewState.observe(viewLifecycleOwner, Observer {
            setImageDetailsViewState(it)
        })
    }

    private fun setImageDetailsViewState(viewState: LoadingDataViewState<ImageDetailsModel>) =
        when (viewState) {
            is LoadingDataViewState.Data -> {
                viewState.data?.let { model ->
                    author_tv.text = model.author
                    camera_tv.text = model.camera ?: ""

                    Glide.with(image_iv)
                        .load(model.fullPicture)
                        .placeholder(R.color.black)
                        .thumbnail(
                            Glide.with(image_iv)
                                .load(model.croppedPicture)
                                .placeholder(R.color.black)
                        )
                        .into(image_iv)
                }

            }
            is LoadingDataViewState.Loading -> {
            }
            is LoadingDataViewState.Error -> {
            }
        }
}