package com.example.gallery.presentation.di.module

import com.example.base.presentation.di.scope.PerFeature
import com.example.gallery.data.network.retrofit.IGalleryRetrofit
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class GalleryNetworkModule {

    @Provides
    @PerFeature
    fun provideRetrofit(retrofit: Retrofit): IGalleryRetrofit =
        retrofit.create(IGalleryRetrofit::class.java)
}