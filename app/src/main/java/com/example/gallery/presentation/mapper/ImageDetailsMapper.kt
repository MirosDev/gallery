package com.example.gallery.presentation.mapper

import com.example.base.data.mapper.BaseMapper
import com.example.gallery.domain.model.ImageDetails
import com.example.gallery.presentation.model.ImageDetailsModel
import javax.inject.Inject

class ImageDetailsMapper @Inject constructor() : BaseMapper<ImageDetails, ImageDetailsModel>() {

    override fun map(from: ImageDetails) =
        ImageDetailsModel(
            from.id,
            from.author,
            from.camera,
            from.tags,
            from.croppedPicture,
            from.fullPicture
        )

    override fun reverse(to: ImageDetailsModel) =
        ImageDetails(
            to.id,
            to.author,
            to.camera,
            to.tags,
            to.croppedPicture,
            to.fullPicture
        )
}