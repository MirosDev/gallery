package com.example.gallery.presentation.view.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.gallery.presentation.model.PictureModel

class GalleryListDiffUtilCallback : DiffUtil.ItemCallback<PictureModel>() {

    override fun areItemsTheSame(oldItem: PictureModel, newItem: PictureModel): Boolean =
         oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: PictureModel, newItem: PictureModel): Boolean =
         oldItem == newItem
}