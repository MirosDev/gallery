package com.example.gallery.presentation.di.component

import com.example.base.presentation.di.component.AppComponentApi
import com.example.base.presentation.di.component.BaseDaggerComponentHolder
import com.example.base.presentation.di.scope.PerFeature
import com.example.gallery.presentation.di.module.GalleryModule
import com.example.gallery.presentation.di.module.GalleryNetworkModule
import com.example.gallery.presentation.di.module.GalleryVMModule
import com.example.gallery.presentation.view.GalleryDetailsFragment
import com.example.gallery.presentation.view.GalleryListFragment
import dagger.Component

@PerFeature
@Component(dependencies = [AppComponentApi::class],
    modules = [GalleryNetworkModule::class, GalleryModule::class, GalleryVMModule::class])
abstract class GalleryComponent : GalleryComponentApi {

    abstract fun inject(fragment: GalleryListFragment)

    abstract fun inject(fragment: GalleryDetailsFragment)

    companion object : BaseDaggerComponentHolder<GalleryComponent>() {

        fun initAndGet(appComponentApi: AppComponentApi): GalleryComponentApi {
            super.incrementInstancesCount()

            if (component == null) {
                synchronized(GalleryComponent::class.java) {
                    if (component == null) {
                        component = DaggerGalleryComponent.builder()
                            .appComponentApi(appComponentApi)
                            .build()
                    }
                }
            }

            return component!!
        }
    }
}