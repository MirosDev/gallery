package com.example.gallery.presentation.model

data class ImagesModel(
    val pictures: List<PictureModel>,
    val page: Int,
    val pageCount: Int,
    val hasMore: Boolean
)