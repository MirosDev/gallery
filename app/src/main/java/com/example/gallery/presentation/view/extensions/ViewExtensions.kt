package com.example.gallery.presentation.view.extensions

import android.content.Intent
import com.example.gallery.presentation.model.PictureModel

fun List<PictureModel>.findPrevImageId(imageId: String): Int {
    forEachIndexed { i, item ->
        if (item.id == imageId) return i.dec()
    }
    return -1
}

fun List<PictureModel>.findNextImageId(imageId: String): Int {
    forEachIndexed { i, item ->
        if (item.id == imageId) return i.inc()
    }
    return -1
}

fun Intent.buildTextIntent(text: String): Intent = apply {
    action = Intent.ACTION_SEND
    putExtra(Intent.EXTRA_TEXT, text)
    type = "text/plain"
}