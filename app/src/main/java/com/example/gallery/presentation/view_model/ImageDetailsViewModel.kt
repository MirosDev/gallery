package com.example.gallery.presentation.view_model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.base.presentation.view_model.BaseSingleObserver
import com.example.base.presentation.view_model.LoadingDataViewState
import com.example.gallery.domain.interactor.GetImageDetailsUseCase
import com.example.gallery.domain.model.ImageDetails
import com.example.gallery.presentation.mapper.ImageDetailsMapper
import com.example.gallery.presentation.model.ImageDetailsModel
import javax.inject.Inject

class ImageDetailsViewModel @Inject constructor(
    private val imageDetailsMapper: ImageDetailsMapper,
    private val getImageDetailsUseCase: GetImageDetailsUseCase
) : ViewModel() {

    val imageDetailsViewState = MutableLiveData<LoadingDataViewState<ImageDetailsModel>>()

    fun loadImageDetails(id: String) {
        imageDetailsViewState.postValue(LoadingDataViewState.Loading(getImageDetails()))
        val observer = object : BaseSingleObserver<ImageDetails>() {

            override fun onSuccess(data: ImageDetails) {
                val dataModel = imageDetailsMapper.map(data)
                imageDetailsViewState.postValue(LoadingDataViewState.Data(dataModel))
            }

            override fun onError(error: Throwable) {
                imageDetailsViewState.postValue(
                    LoadingDataViewState.Error(
                        error,
                        getImageDetails()
                    )
                )
            }
        }

        getImageDetailsUseCase.dispose()
        getImageDetailsUseCase.execute(observer, GetImageDetailsUseCase.Params(id))
    }

    fun getImageDetails() = imageDetailsViewState.value?.data

    override fun onCleared() {
        super.onCleared()
        getImageDetailsUseCase.dispose()
    }
}