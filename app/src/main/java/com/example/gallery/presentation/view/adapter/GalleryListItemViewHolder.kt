package com.example.gallery.presentation.view.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.gallery.R
import com.example.gallery.presentation.model.PictureModel
import kotlinx.android.synthetic.main.gallery_list_item.view.*

class GalleryListItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(itemModel: PictureModel, callback: GalleryListAdapter.Callback?) {
        Glide.with(itemView.context)
            .load(itemModel.croppedPicture)
            .placeholder(R.color.grey)
            .into(itemView.gallery_list_item_im)

        itemView.gallery_list_item_im.setOnClickListener {
            callback?.onItemClick(itemModel)
        }
    }
}