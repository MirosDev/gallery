package com.example.gallery.data.network

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class NetworkProvider @Inject constructor(app: Application) {

    private val cm: ConnectivityManager?
    private val subject = BehaviorSubject.createDefault<Boolean>(false)

    init {
        val networkIntentFilter = IntentFilter(ACTION)
        cm = app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        app.registerReceiver(NetworkChangedReceiver(), networkIntentFilter)
    }

    fun isOnline(): Boolean = subject.value == true

    fun subscribeToNetworkState(): Observable<Boolean> =
        subject.distinctUntilChanged()
            .serialize()

    fun flowNetworkState(): Flowable<Boolean> =
        subject.toFlowable(BackpressureStrategy.LATEST)
            .serialize()

    private fun updateNetworkState() {
        val isConnected = cm?.activeNetworkInfo?.isConnected ?: false
        subject.onNext(isConnected)
    }

    private inner class NetworkChangedReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            updateNetworkState()
        }
    }

    companion object {
        const val ACTION = "android.net.conn.CONNECTIVITY_CHANGE"
    }
}