package com.example.gallery.data.data_store

import com.example.base.presentation.di.scope.PerFeature
import javax.inject.Inject

@PerFeature
class FactoryGalleryDataStore @Inject constructor(
    private val remoteDataStore: IGalleryDataStore
) {

    fun remoteDataStore() = remoteDataStore
}