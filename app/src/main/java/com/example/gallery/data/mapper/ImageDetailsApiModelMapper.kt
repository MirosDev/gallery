package com.example.gallery.data.mapper

import com.example.base.data.mapper.BaseMapper
import com.example.gallery.data.model.ImageDetailsApiModel
import com.example.gallery.domain.model.ImageDetails
import javax.inject.Inject

class ImageDetailsApiModelMapper @Inject constructor() : BaseMapper<ImageDetailsApiModel, ImageDetails>() {

    override fun map(from: ImageDetailsApiModel) =
        ImageDetails(
            from.id,
            from.author,
            from.camera,
            from.tags,
            from.croppedPicture,
            from.fullPicture
        )

    override fun reverse(to: ImageDetails) =
        ImageDetailsApiModel(
            to.id,
            to.author,
            to.camera,
            to.tags,
            to.croppedPicture,
            to.fullPicture
        )
}