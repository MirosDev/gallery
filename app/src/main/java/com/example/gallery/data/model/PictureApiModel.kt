package com.example.gallery.data.model

import com.google.gson.annotations.SerializedName

data class PictureApiModel(
    @SerializedName("id")
    val id: String,
    @SerializedName("cropped_picture")
    val croppedPicture: String
)