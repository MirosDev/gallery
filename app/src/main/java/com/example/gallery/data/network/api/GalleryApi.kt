package com.example.gallery.data.network.api

import com.example.base.data.network.BaseApi
import com.example.gallery.data.model.ImageDetailsApiModel
import com.example.gallery.data.model.ImagesApiModel
import com.example.gallery.data.network.NetworkProvider
import com.example.gallery.data.network.retrofit.IGalleryRetrofit
import io.reactivex.Single
import javax.inject.Inject

class GalleryApi @Inject constructor(
    private val client: IGalleryRetrofit,
    networkProvider: NetworkProvider
) : IGalleryApi, BaseApi(networkProvider) {

    override fun getImages(page: Int): Single<ImagesApiModel> =
        sendRequest {
            client.getImages(page)
        }

    override fun getImageDetails(id: String): Single<ImageDetailsApiModel> =
        sendRequest {
            client.getImageDetails(id)
        }
}