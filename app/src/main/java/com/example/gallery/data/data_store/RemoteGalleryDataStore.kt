package com.example.gallery.data.data_store

import com.example.gallery.data.model.ImagesApiModel
import com.example.gallery.data.model.ImageDetailsApiModel
import com.example.gallery.data.network.api.IGalleryApi
import io.reactivex.Single
import javax.inject.Inject

class RemoteGalleryDataStore @Inject constructor(
    private val api: IGalleryApi
) : IGalleryDataStore {

    override fun getImages(page: Int): Single<ImagesApiModel> =
        api.getImages(page)

    override fun getImageDetails(id: String): Single<ImageDetailsApiModel> =
        api.getImageDetails(id)
}