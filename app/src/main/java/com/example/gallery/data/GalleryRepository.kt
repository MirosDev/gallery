package com.example.gallery.data

import com.example.gallery.data.data_store.FactoryGalleryDataStore
import com.example.gallery.data.mapper.ImageApiModelMapper
import com.example.gallery.data.mapper.ImageDetailsApiModelMapper
import com.example.gallery.domain.model.Images
import com.example.gallery.domain.model.ImageDetails
import com.example.gallery.domain.repository.IGalleryRepository
import io.reactivex.Single
import javax.inject.Inject

class GalleryRepository @Inject constructor(
    private val factory: FactoryGalleryDataStore,
    private val imageApiModelMapper: ImageApiModelMapper,
    private val imageDetailsApiModelMapper: ImageDetailsApiModelMapper
): IGalleryRepository {

    override fun getImages(page: Int): Single<Images> =
        factory.remoteDataStore().getImages(page)
            .map(imageApiModelMapper::map)

    override fun getImageDetails(id: String): Single<ImageDetails> =
        factory.remoteDataStore().getImageDetails(id)
            .map(imageDetailsApiModelMapper::map)
}