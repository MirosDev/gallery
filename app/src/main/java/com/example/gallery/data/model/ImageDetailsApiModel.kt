package com.example.gallery.data.model

import com.google.gson.annotations.SerializedName

data class ImageDetailsApiModel(
    @SerializedName("id")
    val id: String,
    @SerializedName("author")
    val author: String,
    @SerializedName("camera")
    val camera: String?,
    @SerializedName("tags")
    val tags: String,
    @SerializedName("cropped_picture")
    val croppedPicture: String,
    @SerializedName("full_picture")
    val fullPicture: String
)