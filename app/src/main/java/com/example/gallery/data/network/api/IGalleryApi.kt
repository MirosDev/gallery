package com.example.gallery.data.network.api

import com.example.gallery.data.model.ImagesApiModel
import com.example.gallery.data.model.ImageDetailsApiModel
import io.reactivex.Single

interface IGalleryApi {

    fun getImages(page: Int): Single<ImagesApiModel>

    fun getImageDetails(id: String): Single<ImageDetailsApiModel>
}