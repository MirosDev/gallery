package com.example.gallery.data.network.retrofit

import com.example.gallery.data.model.ImagesApiModel
import com.example.gallery.data.model.ImageDetailsApiModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface IGalleryRetrofit {

    @GET("images")
    fun getImages(@Query("page") page: Int): Single<ImagesApiModel>

    @GET("images/{id}")
    fun getImageDetails(@Path("id") id: String): Single<ImageDetailsApiModel>
}