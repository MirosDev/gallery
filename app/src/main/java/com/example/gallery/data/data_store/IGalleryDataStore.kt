package com.example.gallery.data.data_store

import com.example.gallery.data.model.ImagesApiModel
import com.example.gallery.data.model.ImageDetailsApiModel
import io.reactivex.Single

interface IGalleryDataStore {

    fun getImages(page: Int): Single<ImagesApiModel>

    fun getImageDetails(id: String): Single<ImageDetailsApiModel>
}