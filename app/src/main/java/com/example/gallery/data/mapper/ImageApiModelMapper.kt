package com.example.gallery.data.mapper

import com.example.base.data.mapper.BaseMapper
import com.example.gallery.data.model.ImagesApiModel
import com.example.gallery.domain.model.Images
import javax.inject.Inject

class ImageApiModelMapper @Inject constructor(
    private val pictureApiModelMapper: PictureApiModelMapper
): BaseMapper<ImagesApiModel, Images>() {

    override fun map(from: ImagesApiModel) =
        Images(
            pictureApiModelMapper.map(from.pictures),
            from.page,
            from.pageCount,
            from.hasMore
        )

    override fun reverse(to: Images) =
        ImagesApiModel(
            pictureApiModelMapper.reverse(to.pictures),
            to.page,
            to.pageCount,
            to.hasMore
        )
}