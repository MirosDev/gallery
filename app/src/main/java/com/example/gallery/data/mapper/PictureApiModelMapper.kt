package com.example.gallery.data.mapper

import com.example.base.data.mapper.BaseMapper
import com.example.gallery.data.model.PictureApiModel
import com.example.gallery.domain.model.Picture
import javax.inject.Inject

class PictureApiModelMapper @Inject constructor(): BaseMapper<PictureApiModel, Picture>() {

    override fun map(from: PictureApiModel) =
        Picture(
            from.id,
            from.croppedPicture
        )

    override fun reverse(to: Picture) =
        PictureApiModel(
            to.id,
            to.croppedPicture
        )
}