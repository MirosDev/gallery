package com.example.gallery.data.model

import com.google.gson.annotations.SerializedName

data class ImagesApiModel(
    @SerializedName("pictures")
    val pictures: List<PictureApiModel>,
    @SerializedName("page")
    val page: Int,
    @SerializedName("pageCount")
    val pageCount: Int,
    @SerializedName("hasMore")
    val hasMore: Boolean
)