package com.example.gallery.domain.repository

import com.example.gallery.domain.model.Images
import com.example.gallery.domain.model.ImageDetails
import io.reactivex.Single

interface IGalleryRepository {

    fun getImages(page: Int): Single<Images>

    fun getImageDetails(id: String): Single<ImageDetails>
}