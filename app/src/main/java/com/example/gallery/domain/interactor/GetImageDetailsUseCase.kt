package com.example.gallery.domain.interactor

import com.example.base.domain.interactor.SingleUseCase
import com.example.base.domain.interactor.qualifier.IOScheduler
import com.example.base.domain.interactor.scheduler.ThreadScheduler
import com.example.gallery.domain.model.ImageDetails
import com.example.gallery.domain.repository.IGalleryRepository
import io.reactivex.Single
import javax.inject.Inject

class GetImageDetailsUseCase @Inject constructor(
    private val repository: IGalleryRepository,
    @IOScheduler
    private val ioThreadScheduler: ThreadScheduler
): SingleUseCase<ImageDetails, GetImageDetailsUseCase.Params>(ioThreadScheduler, ioThreadScheduler) {

    override fun buildUseCaseObservable(params: Params): Single<ImageDetails> =
        repository.getImageDetails(params.id)

    class Params(val id: String)
}