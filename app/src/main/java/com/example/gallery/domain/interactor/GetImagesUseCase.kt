package com.example.gallery.domain.interactor

import com.example.base.domain.interactor.SingleUseCase
import com.example.base.domain.interactor.qualifier.IOScheduler
import com.example.base.domain.interactor.qualifier.UIScheduler
import com.example.base.domain.interactor.scheduler.ThreadScheduler
import com.example.gallery.domain.model.Images
import com.example.gallery.domain.repository.IGalleryRepository
import io.reactivex.Single
import javax.inject.Inject

class GetImagesUseCase @Inject constructor(
    private val repository: IGalleryRepository,
    @IOScheduler
    private val ioThreadScheduler: ThreadScheduler,
    @UIScheduler
    private val uiThreadScheduler: ThreadScheduler
): SingleUseCase<Images, GetImagesUseCase.Params>(ioThreadScheduler, ioThreadScheduler) {

    override fun buildUseCaseObservable(params: Params): Single<Images> =
        repository.getImages(params.page)

    class Params(val page: Int)
}