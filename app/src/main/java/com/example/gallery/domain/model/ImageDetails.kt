package com.example.gallery.domain.model

data class ImageDetails(
    val id: String,
    val author: String,
    val camera: String?,
    val tags: String,
    val croppedPicture: String,
    val fullPicture: String
)