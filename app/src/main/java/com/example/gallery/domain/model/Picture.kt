package com.example.gallery.domain.model

data class Picture(
    val id: String,
    val croppedPicture: String
)