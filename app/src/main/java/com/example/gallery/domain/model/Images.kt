package com.example.gallery.domain.model

data class Images(
    val pictures: List<Picture>,
    val page: Int,
    val pageCount: Int,
    val hasMore: Boolean
)